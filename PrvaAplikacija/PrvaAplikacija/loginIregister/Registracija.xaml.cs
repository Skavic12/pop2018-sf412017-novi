﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Models;
using PrvaAplikacija.Database;

namespace PrvaAplikacija.loginIregister
{
    /// <summary>
    /// Interaction logic for Registracija.xaml
    /// </summary>
    public partial class Registracija : Window
    {
        public Korisnik korisnik;

        public Registracija()
        {
            InitializeComponent();
            ComboPol.Items.Add(EPol.M);
            ComboPol.Items.Add(EPol.Z);
            ComboTipKorisnika.Items.Add(ETipKorisnika.KORISNIK);
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            korisnik.Sacuvaj();

        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
