﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Database;
using PrvaAplikacija.Models;


namespace PrvaAplikacija.loginIregister
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class login : Window
    {

        public Korisnik korisnik;
        public enum StatusKor { Korisnik, Admi, BezLogovanja }
        public StatusKor statusKor;

        public login()
        {

            InitializeComponent();

            
            this.DataContext = DataContext;
            
            


            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            foreach(Korisnik korisnik in Data.Instance.Korisnici)
            {
                var KorisnikUlg = korisnik;
                if(txtkorime.Text.Equals(KorisnikUlg.Korisnickoime)&& txtlozinka.Text.Equals(KorisnikUlg.Lozinka))
                {
                    this.DialogResult = true;
                    MainWindow m = new MainWindow(KorisnikUlg);
                    m.Show();
                }
                
            }

            
        }

        private bool valicaijaKorisnika()
        {
            foreach(Korisnik korisnik in Data.Instance.Korisnici)
            {
                if(txtkorime.Text.Equals(korisnik.Korisnickoime)&& txtlozinka.Text.Equals(korisnik.Lozinka))
                {
                    return true;
                }

            }
            return false;
        }



        private void Odustani_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
