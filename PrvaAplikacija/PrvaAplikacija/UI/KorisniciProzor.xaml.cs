﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Database;
using PrvaAplikacija.Models;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;

namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for KorisniciProzor.xaml
    /// </summary>
    public partial class KorisniciProzor : Window
    {
        ICollectionView view;
        public KorisniciProzor()
        {
            InitializeComponent();
            DGkorisnici.ItemsSource = Data.Instance.Korisnici;
            DGkorisnici.IsSynchronizedWithCurrentItem = true;

            view = CollectionViewSource.GetDefaultView(Data.Instance.Korisnici);
            view.Filter = CustomFilter;


            DGkorisnici.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DGkorisnici.ItemsSource = view;
            DGkorisnici.IsSynchronizedWithCurrentItem = true;

        }

        private bool CustomFilter(Object obj)
        {
            Korisnik korisnik = obj as Korisnik;
            if (txtPretraga.Text.Equals(String.Empty))
            {
                return korisnik.Active;
            }
            else if (korisnik.Ime.Contains(txtPretraga.Text))
            {
                return korisnik.Active && korisnik.Ime.Contains(txtPretraga.Text);
            }
            else if (korisnik.Prezime.Contains(txtPretraga.Text))
            {
                return korisnik.Active && korisnik.Prezime.Contains(txtPretraga.Text);
            }
            else if (korisnik.Pol.Contains(txtPretraga.Text))
            {
                 return korisnik.Active && korisnik.Pol.Contains(txtPretraga.Text);
            }


            else
            {
                return korisnik.Active && korisnik.Korisnickoime.Contains(txtPretraga.Text);
            }
        }

      


        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            UnosIzmenaKorisnika kor = new UnosIzmenaKorisnika(new Korisnik(), UnosIzmenaKorisnika.Stanje.DODAVANJE);
            kor.ShowDialog();

       

        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Korisnik SelektovaniKorisnik = (Korisnik)DGkorisnici.SelectedItem;
            if (SelektovaniKorisnik != null)
            {
                Korisnik stari = (Korisnik)SelektovaniKorisnik.Clone();
                UnosIzmenaKorisnika UnIzKor = new UnosIzmenaKorisnika(SelektovaniKorisnik, UnosIzmenaKorisnika.Stanje.IZMENA);
                if(UnIzKor.ShowDialog() != true)
                {
                    int indeks = indeksKorisnika(SelektovaniKorisnik.Korisnickoime);
                    Data.Instance.Korisnici[indeks] = stari;
                }
                else
                {
                    SelektovaniKorisnik.Izmena();

                }
                

            }
            else
            {
                MessageBox.Show("Nije sleketovan nijedan Aerodrom");
            }
            DGkorisnici.Items.Refresh();


        }




        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DGkorisnici.SelectedItem;
            if(SelektovaniKorisnik(korisnik)&& korisnik.Active)
            {
                if (MessageBox.Show("Da li ste sigurni", "potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = indeksKorisnika(korisnik.Korisnickoime);
                    Data.Instance.Korisnici[indeks].Active = false;
                    Korisnik SelektovaniKorisnik = (Korisnik)DGkorisnici.SelectedItem;
                    Data.Instance.Korisnici.Remove(SelektovaniKorisnik);
                }

            }

        }

        private int indeksKorisnika(String korisnickoime)
        {
            var indeks = -1;
            foreach(var korisnik in Data.Instance.Korisnici)
            for(int i = 0; i< Data.Instance.Korisnici.Count; i++)
                {
                    if(Data.Instance.Korisnici[i].Korisnickoime.Equals(korisnickoime))
                    {     
                        indeks = i;
                        break;
                    }
                }
            return indeks;
        }

        private bool SelektovaniKorisnik(Korisnik korisnik)
        {
            if(korisnik == null)
            {
                MessageBox.Show("Molimo vas selektujte korisnika");
                return false;
            }
            return true;
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {

        }


        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

    }

}
