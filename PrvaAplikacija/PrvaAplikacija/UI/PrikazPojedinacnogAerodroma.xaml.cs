﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Models;
using PrvaAplikacija.Database;

namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for PrikazPojedinacnogAerodroma.xaml
    /// </summary>
    public partial class PrikazPojedinacnogAerodroma : Window
    {
        public enum Stanje { DODAVANJE,IZMENA};
        public Aerodrom aerodrom;
        public Stanje stanje;
        public Let let;
        public PrikazPojedinacnogAerodroma(Aerodrom aerodrom,Stanje stanje = Stanje.DODAVANJE) 
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.stanje = stanje;

            this.DataContext = aerodrom;

            if (stanje.Equals(Stanje.IZMENA))
            {
                TxtSifra.IsEnabled = false;
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if(stanje.Equals(Stanje.DODAVANJE)&& !ValidacijaSifra(aerodrom.Sifra))
            {
                aerodrom.Sacuvaj();

            }


        }

        private bool ValidacijaSifra(string sifra)
        {
            foreach(Aerodrom aerodrom in Data.Instance.Aerodromi)
            {
                if (aerodrom.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }
    }
}
