﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Models;
using PrvaAplikacija.Database;

namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for LetUnosIzmena.xaml
    /// </summary>
    public partial class LetUnosIzmena : Window
    {

        public enum Stanje { DODAVANJE, IZMENA };
        public Let let;
        public Stanje stanje;
        public Aerodrom aerodrom;
        public AvioKompanija AvioKompanija;

        public LetUnosIzmena(Let let, Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.let = let;
            this.stanje = stanje;
            this.aerodrom = aerodrom;
            this.AvioKompanija = AvioKompanija;

            this.DataContext = let;

            cDestinacija.ItemsSource = Data.Instance.Aerodromi;
            cOdrediste.ItemsSource = Data.Instance.Aerodromi;

            cAvioK.ItemsSource = Data.Instance.AvioKompanija;

        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (stanje.Equals(Stanje.DODAVANJE) && !Validacija(let.Sifra))
            {
                let.Sacuvati();
            }

        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }

        private bool Validacija(String Sifra)    
        {
            foreach(Let let in Data.Instance.Letovi)
            {
                if (let.Sifra.Equals(Sifra))
                {
                    return true;
                }
            }
            return false;

        }
    }
        
}
