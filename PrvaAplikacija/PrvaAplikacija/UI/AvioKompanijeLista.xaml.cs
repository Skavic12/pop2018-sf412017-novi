﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Database;
using System.Collections.ObjectModel;
using PrvaAplikacija.Models;

namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for AvioKompanijeLista.xaml
    /// </summary>
    public partial class AvioKompanijeLista : Window
    {
        
       

        public AvioKompanijeLista(Korisnik ulgKor)
        {
            InitializeComponent();
            DGaviokompanija.ItemsSource = Data.Instance.AvioKompanija;
            DGaviokompanija.IsSynchronizedWithCurrentItem = true;

            DGaviokompanija.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            if (ulgKor.Tipkorisnika.Equals("KORISNIK"))
            {
                BtnDodaj.Visibility = Visibility.Hidden;
                BtnIzmeni.Visibility = Visibility.Hidden;
                BtnObrisi.Visibility = Visibility.Hidden;
                
            }

        }

        private ObservableCollection<AvioKompanija> AktivneKompanije(ObservableCollection<AvioKompanija> avioKompanijas)
        {
            ObservableCollection<AvioKompanija> aktivnakompanija = new ObservableCollection<AvioKompanija>();
            foreach(AvioKompanija avioKompanija in avioKompanijas)
            {
                aktivnakompanija.Add(avioKompanija);
            }

            return aktivnakompanija;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AAvioKompanijaIzmena aav = new AAvioKompanijaIzmena(new AvioKompanija(), AAvioKompanijaIzmena.Stanje.DODAVANJE);
            aav.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            AvioKompanija Selektovanaaivokompanija = (AvioKompanija)DGaviokompanija.SelectedItem;
            if(Selektovanaaivokompanija != null)
            {
                AvioKompanija stari = (AvioKompanija)Selektovanaaivokompanija.Clone();
                AAvioKompanijaIzmena uu = new AAvioKompanijaIzmena(Selektovanaaivokompanija, AAvioKompanijaIzmena.Stanje.IZMENA);
                if(uu.ShowDialog() != true)
                {
                    int indeks = indeksSelektovanogkompanije(Selektovanaaivokompanija.KompaniSifra);
                    Data.Instance.AvioKompanija[indeks] = stari;
                }
                else
                {
                    Selektovanaaivokompanija.Izmena();
                }
                
            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan");
            }
            DGaviokompanija.Items.Refresh();
        

    }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            AvioKompanija avioKompanija = (AvioKompanija)DGaviokompanija.SelectedItem;
            if(SelektovanAvioKompanija(avioKompanija)&& avioKompanija.Active)
            {
                if (MessageBox.Show("Da li ste sigurni da zelite da obrisete avio kompaniju ", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = indeksSelektovanogkompanije(avioKompanija.KompaniSifra);
                    Data.Instance.AvioKompanija[indeks].Active = false;
                    AvioKompanija SelektovanaAvioKompanija = (AvioKompanija)DGaviokompanija.SelectedItem;
                    Data.Instance.AvioKompanija.Remove(SelektovanaAvioKompanija);
                }

            }

        }
        private int indeksSelektovanogkompanije(String KompaniSifra)
        {
            var indeks = -1;
            for (int i = 0; i < Data.Instance.AvioKompanija.Count; i++)
            {
                if (Data.Instance.Letovi[i].Sifra.Equals(KompaniSifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAvioKompanija(AvioKompanija avioKompanija)
        {
            if (avioKompanija == null)
            {
                MessageBox.Show("Nije selektovan let");
                return false;
            }
            return true;
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
