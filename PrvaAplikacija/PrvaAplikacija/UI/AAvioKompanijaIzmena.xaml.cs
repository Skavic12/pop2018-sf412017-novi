﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Models;
using PrvaAplikacija.Database;

namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for AAvioKompanijaIzmena.xaml
    /// </summary>
    public partial class AAvioKompanijaIzmena : Window
    {
        public enum Stanje { DODAVANJE, IZMENA};
        public AvioKompanija avioKompanija;
        public Stanje stanje;

        public AAvioKompanijaIzmena(AvioKompanija avioKompanija,Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.avioKompanija = avioKompanija;
            this.stanje = stanje;

            this.DataContext = avioKompanija;
            if (stanje.Equals(Stanje.IZMENA))
            {
                TxtSifrakompanije.IsEnabled = false;
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if(stanje.Equals(Stanje.DODAVANJE)&& !ValidacijaKOmpanije(avioKompanija.KompaniSifra))
            {
                avioKompanija.Sacuvaj();
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private bool ValidacijaKOmpanije(string kompanisifra)
        {
            foreach (AvioKompanija avioKompanija in Data.Instance.AvioKompanija)
            {
                if (avioKompanija.KompaniSifra.Equals(kompanisifra))
                {
                    return true;
                }
            }
            return false;
        }
    }

}
