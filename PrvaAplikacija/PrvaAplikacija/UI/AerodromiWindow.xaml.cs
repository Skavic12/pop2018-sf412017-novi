﻿using PrvaAplikacija.Database;
using PrvaAplikacija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for AerodromiWindow.xaml
    /// </summary>
    public partial class AerodromiWindow : Window
    {
        public AerodromiWindow(Korisnik logovanKorisnik)
        {
            InitializeComponent();
            //Reload();
            DGAerodromi.ItemsSource = Data.Instance.Aerodromi;
            DGAerodromi.IsSynchronizedWithCurrentItem = true;

            DGAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            if (logovanKorisnik.Equals("KORISNIK"))
            {
                BtnDodajAerodrom.Visibility = Visibility.Hidden;
                BtnIzmeniAerodrom.Visibility = Visibility.Hidden;
                BtnObrisiAerodrom.Visibility = Visibility.Hidden;
            }


        }

        private bool CustomFilter(object obj)
        {
            Aerodrom aerodrom = obj as Aerodrom;
            if (txtSearch.Text.Equals(String.Empty))
                return !aerodrom.Active;
            else
                return !aerodrom.Active && aerodrom.Naziv.Contains(txtSearch.Text);
        }

        //refresh
       /* private void Reload()
        {
            if(LBAerodromi.HasItems)
            {
                LBAerodromi.Items.Clear();
            }
            foreach (var aerodromi in Data.Instance.Aerodromi)
            {
                if(aerodromi.Aktivan)
                {
                    LBAerodromi.Items.Add(aerodromi);
                }
            }
        } */

        private void BtnDodajAerodrom_Click(object sender, RoutedEventArgs e)
        {

            PrikazPojedinacnogAerodroma Paerodrom = new PrikazPojedinacnogAerodroma(new Aerodrom());
            Paerodrom.ShowDialog();

        }

        private void BtnObrisiAerodrom_Click(object sender, RoutedEventArgs e)
        { 
            Aerodrom aerodrom = (Aerodrom)DGAerodromi.SelectedItem;
            if(SelektovanAerodrom(aerodrom)&& aerodrom.Active)
            {
                if (MessageBox.Show("Da li ste sigurni", "potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksAerodrom(aerodrom.Sifra);
                    Data.Instance.Aerodromi[indeks].Active = false;
                    Aerodrom selektovanAerodrom = (Aerodrom)DGAerodromi.SelectedItem;
                    Data.Instance.Aerodromi.Remove(selektovanAerodrom);
                }
            }
             
        }

        private int IndeksAerodrom(String sifra)
        {
            var indeks = -1;
            foreach(var aerodrom in Data.Instance.Aerodromi)
            for(int i = 0; i < Data.Instance.Aerodromi.Count; i++)
            {
                if(Data.Instance.Aerodromi[i].Sifra.Equals(sifra)) {
                        indeks = i;
                        break;
                    }
            }
            return indeks;
        }

        private bool SelektovanAerodrom(Aerodrom aerodrom)
        {
            if(aerodrom == null)
            {
                MessageBox.Show("Molimo vas selektujte aerodrom");
                return false;
            }
            return true;
        }

        private void BtnIzmeniAerodrom_Click(object sender, RoutedEventArgs e)
        { 
            Aerodrom selektovanAerodrom = (Aerodrom)DGAerodromi.SelectedItem;
            if(selektovanAerodrom != null)
            {
                Aerodrom stari = (Aerodrom) selektovanAerodrom.Clone();
                PrikazPojedinacnogAerodroma prikazAerodroma = new PrikazPojedinacnogAerodroma(selektovanAerodrom, PrikazPojedinacnogAerodroma.Stanje.IZMENA);
                if(prikazAerodroma.ShowDialog() != true)
                {
                    int indeks = IndeksAerodrom(selektovanAerodrom.Sifra);
                    Data.Instance.Aerodromi[indeks] = stari;
                }
                else
                {
                    selektovanAerodrom.Izmena();
                }
                
            }
            else
            {
                MessageBox.Show("Nije sleketovan nijedan Aerodrom");
            }
            DGAerodromi.Items.Refresh();
            
        } 
    }
}
