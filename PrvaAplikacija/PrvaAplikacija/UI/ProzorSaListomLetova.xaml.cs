﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Database;
using PrvaAplikacija.Models;
using System.Collections.ObjectModel;
using System.ComponentModel;
using PrvaAplikacija.KarteProzor;
namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for ProzorSaListomLetova.xaml
    /// </summary>
    public partial class ProzorSaListomLetova : Window
    {
        ICollectionView view;

        public Korisnik korisnik;

        public Korisnik ulogovaniKorisnik;
        

        public ProzorSaListomLetova(Korisnik UlKorisnik)
        {
            InitializeComponent();

            ulogovaniKorisnik = UlKorisnik;
            
            

            DGletovi.ItemsSource = Data.Instance.Letovi;
            DGletovi.IsSynchronizedWithCurrentItem = true;

            view = CollectionViewSource.GetDefaultView(Data.Instance.Letovi);
            view.Filter = CustomFilter;


            DGletovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DGletovi.ItemsSource = view;
            DGletovi.IsSynchronizedWithCurrentItem = true;

            if (ulogovaniKorisnik.Tipkorisnika.Equals("KORISNIK"))
            {
                BtnDodaj.Visibility = Visibility.Hidden;
                BtnIzmeni.Visibility = Visibility.Hidden;
                BtnObrisi.Visibility = Visibility.Hidden;

            }


        }
        private ObservableCollection<Let> AktivniLetovi(ObservableCollection<Let> Letovi)
        {
            ObservableCollection<Let> aktivniLetovi = new ObservableCollection<Let>();
            foreach (Let let in Letovi)
            {
                aktivniLetovi.Add(let);
            }
            return aktivniLetovi;

        }

        



        private bool CustomFilter(Object obj)
        {
            Let let = obj as Let;
            if (txtPretraga.Text.Equals(String.Empty))
            {
                return let.Active;
            }
            else if (let.Odrediste.Contains(txtPretraga.Text))
            {
                return let.Active && let.Odrediste.Contains(txtPretraga.Text);
            }
            else if (let.Sifra.Contains(txtPretraga.Text))
            {
                return let.Active && let.Sifra.Contains(txtPretraga.Text);
            }

            else if (let.AvioKOmpanija.Contains(txtPretraga.Text))
            {
                return let.Active && let.AvioKOmpanija.Contains(txtPretraga.Text);
            }


            else
            {
                return let.Active && let.Destinacija.Contains(txtPretraga.Text);
            }


        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            
            LetUnosIzmena unsLet = new LetUnosIzmena(new Let(), LetUnosIzmena.Stanje.DODAVANJE);
            unsLet.ShowDialog();
            

        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Let SelektovanLet = (Let)DGletovi.SelectedItem;
            if (SelektovanLet != null)
            {
                Let stari = (Let) SelektovanLet.Clone();
                LetUnosIzmena unsl = new LetUnosIzmena(SelektovanLet, LetUnosIzmena.Stanje.IZMENA);
                if(unsl.ShowDialog() != true)
                {
                    int indeks = indeksSelektovanogLeta(SelektovanLet.Sifra);
                    Data.Instance.Letovi[indeks] = stari;
                    

                }
                else
                {
                    MessageBox.Show("Nije selektovan nijedan Aerodrom");
                }
                DGletovi.Items.Refresh();
                   
            }
            

        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DGletovi.SelectedItem;
            if (SelektovanLet(let)&& let.Active)
            {
                if(MessageBox.Show("Da li ste sigurni da zelite da obrisete let ", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = indeksSelektovanogLeta(let.Sifra);
                    Data.Instance.Letovi[indeks].Active = false;
                    Let selektovanLet = (Let)DGletovi.SelectedItem;
                    Data.Instance.Letovi.Remove(selektovanLet);
                }
            }


        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {


        }

        private int indeksSelektovanogLeta(String sifra)
        {
            var indeks = -1;
            for(int i = 0; i<Data.Instance.Letovi.Count; i++)
            {
                if (Data.Instance.Letovi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanLet(Let let)
        {
            if(let == null)
            {
                MessageBox.Show("Nije selektovan let");
                return false;
            }
            return true;
        }

        private void TxtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnKupovina_Click(object sender, RoutedEventArgs e)
        {
            
            

            Let SelektovanLet = (Let)DGletovi.SelectedItem;
            if (SelektovanLet != null)


            {
                Let stari = (Let)SelektovanLet.Clone();
                KupovinaKarte karkup = new KupovinaKarte(SelektovanLet , ulogovaniKorisnik);
                if (karkup.ShowDialog() != true)
                {
                    int indeks = indeksSelektovanogLeta(SelektovanLet.Sifra);
                    Data.Instance.Letovi[indeks] = stari;


                }

            }
            else
            {
                MessageBox.Show("Niste nista");
            }
        }
    }
}
