﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Database;
using System.Collections.ObjectModel;
using PrvaAplikacija.Models;
using System.ComponentModel;


namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for AvioniPrikaz.xaml
    /// </summary>
    public partial class AvioniPrikaz : Window
    {
        ICollectionView view;
        public AvioniPrikaz(Korisnik ulogovan)
        {
            InitializeComponent();
            DGavion.ItemsSource = Data.Instance.Avions;
            DGavion.IsSynchronizedWithCurrentItem = true;

            view = CollectionViewSource.GetDefaultView(Data.Instance.Avions);
            view.Filter = CustomFilter;
            DGavion.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DGavion.ItemsSource = view;
            DGavion.IsSynchronizedWithCurrentItem = true;

            if (ulogovan.Tipkorisnika.Equals("KORISNIK"))
            {
                BtnDodaj.Visibility = Visibility.Hidden;
                BtnIzmeni.Visibility = Visibility.Hidden;
                BtnObrisi.Visibility = Visibility.Hidden;

            }


        }
        private bool CustomFilter(Object obj)
        {
            Avion avion = obj as Avion;
            if (txtPretraga.Text.Equals(String.Empty))
            {
                return avion.Aktivan;
            }
            else if (avion.BrojLeta.Contains(txtPretraga.Text))
            {
                return avion.Aktivan && avion.BrojLeta.Contains(txtPretraga.Text);
            }
            else if (avion.NazivAvioK.Contains(txtPretraga.Text))
            {
                return avion.Aktivan && avion.NazivAvioK.Contains(txtPretraga.Text);
            }
            

            else
            {
                return avion.Aktivan && avion.Pilot.Contains(txtPretraga.Text);
            }


        }











        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AvionIzmenaIUnos avionIzmenaIUnos = new AvionIzmenaIUnos(new Avion(), AvionIzmenaIUnos.Stanje.DODAVANJE);
            avionIzmenaIUnos.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Avion SelektovanAvion = (Avion)DGavion.SelectedItem;
            if(SelektovanAvion != null)
            {
                Avion stari = (Avion)SelektovanAvion.Clone();
                AvionIzmenaIUnos av = new AvionIzmenaIUnos(SelektovanAvion, AvionIzmenaIUnos.Stanje.IZMENA);
                if(av.ShowDialog()!= true)
                {
                    int indeks = indeksSelektovanogAviona(SelektovanAvion.BrojLeta);
                    Data.Instance.Avions[indeks] = stari;
                }
                else
                {
                    SelektovanAvion.izmena();

                }
                
            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan Aerodrom");

            }

            DGavion.Items.Refresh();
            
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = (Avion)DGavion.SelectedItem;
            if(SelektovanAviona(avion)&& avion.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni da zelite da obrisete avio  ", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = indeksSelektovanogAviona(avion.NazivAvioK);
                    Data.Instance.Avions[indeks].Aktivan = false;
                    Avion SelektovanAvion = (Avion)DGavion.SelectedItems;
                    Data.Instance.Avions.Remove(SelektovanAvion);
                }

            }

        }
        private int indeksSelektovanogAviona(String nazivAviona)
        {
            var indeks = -1;
            for (int i = 0; i < Data.Instance.AvioKompanija.Count; i++)
            {
                if (Data.Instance.Avions[indeks].NazivAvioK.Equals(nazivAviona))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAviona(Avion avion)
        {
            if (avion == null)
            {
                MessageBox.Show("Nije selektovan let");
                return false;
            }
            return true;
        }


        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void BtnPretraga_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TxtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
