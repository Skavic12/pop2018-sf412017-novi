﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Models;
using PrvaAplikacija.Database;

namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for UnosIzmenaKorisnika.xaml
    /// </summary>
    public partial class UnosIzmenaKorisnika : Window
    {
        public enum Stanje { DODAVANJE , IZMENA ,REGISTRACIJA};
        public enum combo {ADMIN,KORISNIK }
        public Korisnik korisnik;
        public Stanje stanje;

        public UnosIzmenaKorisnika(Korisnik korisnik,Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.stanje = stanje;

            this.DataContext = korisnik;
            ComboPol.Items.Add(EPol.M);
            ComboPol.Items.Add(EPol.Z);

            if (stanje.Equals(Stanje.REGISTRACIJA))
            {
                ComboTipKorisnika.Items.Add(ETipKorisnika.KORISNIK);
            }
            else
            {
                ComboTipKorisnika.Items.Add(ETipKorisnika.ADMIN);

                ComboTipKorisnika.Items.Add(ETipKorisnika.KORISNIK);
            }
            if (stanje.Equals(Stanje.IZMENA))
            {
                txtKorisnickoIme.IsEnabled = false;
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if(stanje.Equals(Stanje.DODAVANJE))
            {
                korisnik.Sacuvaj();

            }
            else if (stanje.Equals(Stanje.REGISTRACIJA))
            {
                korisnik.Sacuvaj();
            }

        }
        private bool ValidacijaKorisnickogImena(string korisnickoime)
        {
            foreach (Korisnik korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.Korisnickoime.Equals(korisnickoime))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
