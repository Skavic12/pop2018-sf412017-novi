﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Models;
using PrvaAplikacija.Database;

namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for AvionIzmenaIUnos.xaml
    /// </summary>
    public partial class AvionIzmenaIUnos : Window
    {
        public enum Stanje { DODAVANJE,IZMENA};
        public Avion avion;
        public Stanje stanje;

        public AvionIzmenaIUnos(Avion avion,Stanje stanje = Stanje.DODAVANJE)
        {
            InitializeComponent();
            this.avion = avion;
            this.stanje = stanje;

            this.DataContext = avion;

            ComboNazivAvioKompanije.ItemsSource = Data.Instance.AvioKompanija;

            if (stanje.Equals(Stanje.IZMENA))
            {
                TxtBrojLeta.IsEnabled = false;
                
            }
        }


        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (stanje.Equals(Stanje.DODAVANJE))
            {
                avion.Sacuvaj();
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
