﻿using PrvaAplikacija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrvaAplikacija.UI
{
    /// <summary>
    /// Interaction logic for IzmeniAerodromiWindow.xaml
    /// </summary>
    public partial class IzmeniAerodromiWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA}
        private Opcija opcija;
        private Aerodrom aerodrom;
        public IzmeniAerodromiWindow(Aerodrom aerodrom, Opcija opcija)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.opcija = opcija;

            TxtSifra.Text = aerodrom.Sifra;
            TxtGrad.Text = aerodrom.Grad;
            TxtNaziv.Text = aerodrom.Naziv;

            if(opcija.Equals(Opcija.IZMENA))
            {
                TxtSifra.IsEnabled = false;
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
