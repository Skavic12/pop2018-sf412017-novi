﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.loginIregister;
using PrvaAplikacija.UI;
using PrvaAplikacija.Database;
using PrvaAplikacija.Models;

namespace PrvaAplikacija
{
    /// <summary>
    /// Interaction logic for GlavniProzor.xaml
    /// </summary>
    public partial class GlavniProzor : Window

    {

        public Korisnik korisnik;
        
        public GlavniProzor()
        {
            InitializeComponent();
            this.korisnik = korisnik;

            DGletovi.ItemsSource = Database.Data.Instance.Letovi;
            DGletovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);


        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            login log = new login();
            log.ShowDialog();

        }



        private void AvionBTN_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AvioKompanijabtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RegisterBTN_Click(object sender, RoutedEventArgs e)
        {
            UnosIzmenaKorisnika korg = new UnosIzmenaKorisnika(new Korisnik(), UnosIzmenaKorisnika.Stanje.REGISTRACIJA);
            korg.ShowDialog(); 
        }
    }
}
