﻿CREATE TABLE [dbo].[Aerodrom] (
    [S_aeo]   VARCHAR(50)     NOT NULL,
    [naz_aeo] VARCHAR (30) NULL,
    [grad]    VARCHAR (30) NULL,
    PRIMARY KEY CLUSTERED ([S_aeo] ASC)
);

