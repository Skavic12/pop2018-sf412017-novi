﻿CREATE TABLE [dbo].[Korisnik] (
    [ime]     VARCHAR (20) NULL,
    [prezime] VARCHAR (20) NULL,
    [Kor_ime] VARCHAR (20) NOT NULL,
    [Loz]     VARCHAR (20) NULL,
    [adresa]  VARCHAR (20) NULL,
    [pol]     VARCHAR (2)  NULL,
    [tip_kor] VARCHAR (20) NULL, 
    CONSTRAINT [PK_Korisnik] PRIMARY KEY ([Kor_ime])
);

