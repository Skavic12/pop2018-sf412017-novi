﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Models;
using PrvaAplikacija.Database;
using PrvaAplikacija.KarteProzor;
using PrvaAplikacija.Database;

namespace PrvaAplikacija.KarteProzor
{
    /// <summary>
    /// Interaction logic for KupovinaKarte.xaml
    /// </summary>
    public partial class KupovinaKarte : Window
    {
        public Karte karte;
        public Avion avion;
        public Let let;
        public Korisnik korisnik;

        public enum tipKarte { Povratna, Nepovratna };
        private tipKarte TipKarte;

        public Korisnik ulogovaniKorisnik;

        public AvioKompanija avkomp;

        public String SifraKompanije;


        public KupovinaKarte(Let let, Korisnik ulKorisnik)
        {
            InitializeComponent();

            

            this.let = let;

            ulogovaniKorisnik = ulKorisnik;

            this.DataContext = let;


            KomboTipKarte.Items.Add(tipKarte.Povratna);
            KomboTipKarte.Items.Add(tipKarte.Nepovratna);


            KomboBoxSedista.ItemsSource = Data.Instance.Sedistas;

            
            txtKorisnik.Text = ulogovaniKorisnik.Korisnickoime;
            CenaLabela.Text = let.Cena.ToString();
            Odlbl.Text = let.Odrediste;
            lblDo.Text= let.Destinacija;

            foreach(AvioKompanija komp in Data.Instance.AvioKompanija)
            {
                if (komp.NazivKompanije.Equals(let.AvioKOmpanija))
                {
                    LblKomp.Text = komp.KompaniSifra;
                    SifraKompanije = komp.KompaniSifra;
                }
            }

            


            
            txtKorisnik.IsEnabled = false;
            CenaLabela.IsEnabled = false;
            Odlbl.IsEnabled = false;
            lblDo.IsEnabled = false;
            LblKomp.IsEnabled = false;






        }

        private void BtnKupiKartu_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            Karte kart = new Karte();
            kart.SifraLeta = let.Sifra;
            kart.NazivPutnika = ulogovaniKorisnik.Prezime + " " + ulogovaniKorisnik.Ime;
            kart.ZauzetoSediste = KomboBoxSedista.SelectedItem.ToString();
            kart.UkupnaCena = let.Cena;
            kart.TipKarte = KomboTipKarte.SelectedItem.ToString();
            kart.Kompanija = SifraKompanije;
            kart.Active = true;

            kart.Sacuvati();


        }

        private void BtnPonisti_Click(object sender, RoutedEventArgs e)
        {

        }


        private bool Provera(String SifraLeta, String Sediste)
        {
            foreach(Karte karte in Data.Instance.Kartes)
            {
                if (karte.SifraLeta.Equals(SifraLeta))
                {
                    return true;
                }
            }

            return false;
        }

    }
}
