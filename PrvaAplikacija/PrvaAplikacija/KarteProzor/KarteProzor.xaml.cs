﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrvaAplikacija.Models;
using System.ComponentModel;
using PrvaAplikacija.Database;

namespace PrvaAplikacija.KarteProzor
{
    /// <summary>
    /// Interaction logic for KarteProzor.xaml
    /// </summary>
    public partial class KarteProzor : Window
    {
        ICollectionView view;
        public KarteProzor(Korisnik ulogovan)
        {
            InitializeComponent();
            DGKarte.ItemsSource = Data.Instance.Avions;
            DGKarte.IsSynchronizedWithCurrentItem = true;


            view = CollectionViewSource.GetDefaultView(Data.Instance.Kartes);
            DGKarte.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DGKarte.ItemsSource = view;
            DGKarte.IsSynchronizedWithCurrentItem = true;



        }

        private void BtnPonisti_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnKupiKartu_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
