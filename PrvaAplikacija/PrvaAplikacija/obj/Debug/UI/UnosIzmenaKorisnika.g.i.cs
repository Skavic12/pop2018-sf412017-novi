﻿#pragma checksum "..\..\..\UI\UnosIzmenaKorisnika.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3319BC96DAE1237DE893F1935EA39CA902C84EB4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using PrvaAplikacija.UI;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PrvaAplikacija.UI {
    
    
    /// <summary>
    /// UnosIzmenaKorisnika
    /// </summary>
    public partial class UnosIzmenaKorisnika : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label NAZIV;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblIme;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPrezime;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblKorisnickoIme;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbllozinka;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPol;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAdresa;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbltipKorisnika;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtIme;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtPrezime;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtKorisnickoIme;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtLozinka;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtAdresa;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboPol;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboTipKorisnika;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblemail;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Txtemail;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSacuvaj;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnNazad;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PrvaAplikacija;component/ui/unosizmenakorisnika.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.NAZIV = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.lblIme = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.lblPrezime = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.lblKorisnickoIme = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.lbllozinka = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.lblPol = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.lblAdresa = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.lbltipKorisnika = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.TxtIme = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.TxtPrezime = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.txtKorisnickoIme = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.TxtLozinka = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.TxtAdresa = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.ComboPol = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 15:
            this.ComboTipKorisnika = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 16:
            this.lblemail = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.Txtemail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.BtnSacuvaj = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
            this.BtnSacuvaj.Click += new System.Windows.RoutedEventHandler(this.BtnSacuvaj_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.BtnNazad = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\..\UI\UnosIzmenaKorisnika.xaml"
            this.BtnNazad.Click += new System.Windows.RoutedEventHandler(this.BtnNazad_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

