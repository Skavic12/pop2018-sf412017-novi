﻿#pragma checksum "..\..\..\UI\IzmeniAerodromiWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "B869E3D0239473DCBACD222E54832C0F39C921F4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using PrvaAplikacija.UI;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PrvaAplikacija.UI {
    
    
    /// <summary>
    /// IzmeniAerodromiWindow
    /// </summary>
    public partial class IzmeniAerodromiWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblSifra;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtSifra;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblNaziv;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtNaziv;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblGrad;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtGrad;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSacuvaj;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnOdustani;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PrvaAplikacija;component/ui/izmeniaerodromiwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LblSifra = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.TxtSifra = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.LblNaziv = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.TxtNaziv = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.LblGrad = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.TxtGrad = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.BtnSacuvaj = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
            this.BtnSacuvaj.Click += new System.Windows.RoutedEventHandler(this.BtnSacuvaj_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.BtnOdustani = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\..\UI\IzmeniAerodromiWindow.xaml"
            this.BtnOdustani.Click += new System.Windows.RoutedEventHandler(this.BtnOdustani_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

