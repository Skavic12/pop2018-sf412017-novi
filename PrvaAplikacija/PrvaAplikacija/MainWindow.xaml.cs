﻿using PrvaAplikacija.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PrvaAplikacija.loginIregister;
using PrvaAplikacija.Models;
using PrvaAplikacija.Database;
using PrvaAplikacija.KarteProzor;
using System.Collections.ObjectModel;

namespace PrvaAplikacija
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Korisnik korisnik;

        public Korisnik ulogovanKorisnik;
        

        public MainWindow(Korisnik ulogovan)
        {
            InitializeComponent();

            ulogovanKorisnik = ulogovan;

            DGletovi.ItemsSource = Database.Data.Instance.Letovi;
            DGletovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            this.korisnik = korisnik;
            

            
            lblUSERNAME.Content = ulogovanKorisnik.Korisnickoime + " "+ ulogovanKorisnik.Tipkorisnika;

            if (ulogovanKorisnik.Tipkorisnika.Equals("KORISNIK"))
            {
                BtnKorisnici.Visibility = Visibility.Hidden;
                btnAvion.Visibility = Visibility.Hidden;
            }
        
            
        }

        

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            
            AerodromiWindow aw = new AerodromiWindow(ulogovanKorisnik);
            aw.Show();
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            
            ProzorSaListomLetova PrLet = new ProzorSaListomLetova(ulogovanKorisnik);
            PrLet.Show();

        }

        private void BtnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            
            KorisniciProzor korp = new KorisniciProzor();
            korp.Show();
        }


        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            
            AvioKompanijeLista aviokompanija = new AvioKompanijeLista(ulogovanKorisnik);
            aviokompanija.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            AvioniPrikaz Aprikaz = new AvioniPrikaz(ulogovanKorisnik);
            Aprikaz.Show();
        }

        private void BtnKupovina_Click(object sender, RoutedEventArgs e)
        {

            
            KarteProzor.KarteProzor kar = new KarteProzor.KarteProzor(korisnik);
            kar.Show();
        }
    }
}
