﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
namespace PrvaAplikacija.Models
{
    public class Avion : INotifyPropertyChanged, ICloneable
    {
        public void Sacuvaj()
        {
            using(SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO AvionBaza(pilot,s_leta,Sed_B,Sed_E,s_kom,status)" + "VALUES(@pilot,@BrojLeta,@SedistaBiznisKlase,@SedistaEkonomskeKlase,@NazivAvioK,@Aktivan)";

                command.Parameters.Add(new SqlParameter(@"pilot",this.pilot));
                command.Parameters.Add(new SqlParameter(@"BrojLeta",this.BrojLeta));
                command.Parameters.Add(new SqlParameter(@"SedistaBiznisKlase",this.SedistaBiznisKlase));
                command.Parameters.Add(new SqlParameter(@"SedistaEkonomskeKlase",this.SedistaEkonomskeKlase));
                command.Parameters.Add(new SqlParameter(@"NazivAvioK",this.NazivAvioK));
                command.Parameters.Add(new SqlParameter(@"Aktivan",true));

                command.ExecuteNonQuery();

            }
            Database.Data.Instance.UcitajSveAvione();
        }

        public void izmena()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE AvionBaza SET pilot=@pilot,s_leta=@BrojLeta,Sed_B=@SedistaBiznisKlase,Sed_E=@SedistaEkonomskeKlase,s_kom=@NazivAvioK ,status=@Aktivan where id=@Id ";

                command.Parameters.Add(new SqlParameter(@"Id", this.Id));
                command.Parameters.Add(new SqlParameter(@"pilot", this.pilot));
                command.Parameters.Add(new SqlParameter(@"BrojLeta", this.BrojLeta));
                command.Parameters.Add(new SqlParameter(@"SedistaBiznisKlase", this.SedistaBiznisKlase));
                command.Parameters.Add(new SqlParameter(@"SedistaEkonomskeKlase", this.SedistaEkonomskeKlase));
                command.Parameters.Add(new SqlParameter(@"NazivAvioK", this.NazivAvioK));
                command.Parameters.Add(new SqlParameter(@"Aktivan", true));

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveAvione();

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public Avion()
        {
            Aktivan = true;
        }

        private String pilot;

        public string Pilot
        {
            get { return pilot; }
            set { pilot = value; OnProperyChanged("pilot"); }
        }
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("Id"); }
        }


        private String brojLeta;

        public string BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; OnProperyChanged("BrojLeta"); }
        }

        private String sedistaBiznisKlase;

        public string SedistaBiznisKlase
        {
            get { return sedistaBiznisKlase; }
            set { sedistaBiznisKlase = value; OnProperyChanged("SedistaBiznisKlase"); }
        }

        private String sedistaEkonomskeKlase;

        public string SedistaEkonomskeKlase
        {
            get { return sedistaEkonomskeKlase; }
            set { sedistaEkonomskeKlase = value; OnProperyChanged("SedistaEkonomskeKlase"); }
        }

        private String nazivAvioK;

        public string NazivAvioK
        {
            get { return nazivAvioK; }
            set { nazivAvioK = value; OnProperyChanged("NazivAvioK"); }

        }



        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }

        }

        public object Clone()
        {
            return new Avion
            {
                Id = this.Id,
                pilot = this.pilot,
                brojLeta = this.brojLeta,
                sedistaBiznisKlase = this.sedistaBiznisKlase,
                sedistaEkonomskeKlase = this.sedistaEkonomskeKlase,
                nazivAvioK = this.nazivAvioK,
                Aktivan = this.Aktivan
            };
        }

    }
}
