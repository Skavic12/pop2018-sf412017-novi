﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;

namespace PrvaAplikacija.Models
{
    public class Aerodrom : INotifyPropertyChanged, ICloneable
    {

        public void Sacuvaj()
        {
            using( SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO AerodromiBaze(s_aeo ,Naziv,Grad,status)" + "VALUES (@Sifra , @Naziv , @Grad, @Active)";

                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("@Grad", this.Grad));
                command.Parameters.Add(new SqlParameter("@Active", true));

                command.ExecuteNonQuery();

            }
            Database.Data.Instance.UcitatiSveAerodrome();
        }

        public void Izmena()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update AerodromiBaze set s_aeo=@Sifra , Naziv=@Naziv ,Grad=@grad,status=@Active where id=@id";

                comm.Parameters.Add(new SqlParameter(@"id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@Naziv", this.Naziv));
                comm.Parameters.Add(new SqlParameter("@Grad", this.Grad));
                comm.Parameters.Add(new SqlParameter("@Active", this.Active));

                comm.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Exception {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitatiSveAerodrome();

        }


        public Aerodrom()
        {
            active = true;
        }

        private String sifra;

        public String Sifra
        {
            get { return sifra; }
            set { sifra = value; OnProperyChanged("Sifra"); }
        }

        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("id"); }
        }
        
     

        private String naziv;

        public String Naziv
        {
            get { return naziv; }
            set { naziv = value; OnProperyChanged("Naziv"); }
        }

        private String grad;

        public String Grad
        {
            get { return grad; }
            set { grad = value; OnProperyChanged("Grad"); }
        }

        private bool active;

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"{Sifra}";
        }

        public object Clone()
        {
            Aerodrom aerodrom = new Aerodrom()
            {
                Sifra = this.Sifra,
                Grad = this.Grad,
                Naziv = this.Naziv,
                Active = this.Active
            };
            return aerodrom;
        }
    }
}
