﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace PrvaAplikacija.Models
{
    public class AvioKompanija : INotifyPropertyChanged, ICloneable
    {
        public void Sacuvaj()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO AvioKompanijeBaze(S_kom , naz_kom ,status)" + "VALUES(@KompaniSifra,@NazivKompanije,@Active)";

                command.Parameters.Add(new SqlParameter("@KompaniSifra", this.KompaniSifra));
                command.Parameters.Add(new SqlParameter("@NazivKompanije",this.NazivKompanije));
                command.Parameters.Add(new SqlParameter("@Active", true));

                command.ExecuteNonQuery();
            }
            Database.Data.Instance.UcitajSveAviokompanije();
            
        }

        public void Izmena()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "UPDATE AvioKompanijeBaze SET S_kom=@KompaniSifra , naz_kom=@NazivKompanije , status=@Active where id=@Id";

                comm.Parameters.Add(new SqlParameter(@"Id", this.Id));
                comm.Parameters.Add(new SqlParameter(@"KompaniSifra", this.KompaniSifra));
                comm.Parameters.Add(new SqlParameter(@"NazivKompanije", this.NazivKompanije));
                comm.Parameters.Add(new SqlParameter(@"Active", this.Active));
                comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveAviokompanije();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnProperyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }

        }

        private String kompanisifra;
        public String KompaniSifra
        {
            get { return kompanisifra; }
            set { kompanisifra = value; OnProperyChanged("KompaniSifra"); }
        }

        private String nazivkomanije;
        public String NazivKompanije
        {
            get { return nazivkomanije; }
            set
            {
                nazivkomanije = value; OnProperyChanged("NazivKompanije");
            }
        }

        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("Id"); }
        }



        private bool active;

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public object Clone()
        {
            return new AvioKompanija
            {
                Id = this.Id,
                KompaniSifra = this.KompaniSifra,
                NazivKompanije = this.NazivKompanije,
                Active = this.Active
            };
        }





        public override string ToString()
        {
            return $"{KompaniSifra}";
        }

    }
}