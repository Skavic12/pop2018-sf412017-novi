﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data.SqlClient;

namespace PrvaAplikacija.Models
{
     public class Sedista : INotifyPropertyChanged,ICloneable
    {

        public event PropertyChangedEventHandler PropertyChanged;


        public void OnProperyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }

        }



        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("ïd"); }
        }

        private string broj;
        public String Broj
        {
            get { return broj; }
            set { broj = value;OnProperyChanged("Broj"); }
        }

        


        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        public object Clone()
        {
            return new Sedista
            {
                Id = this.Id,
                Broj = this.Broj,
                
                
            };
        }



        public override string ToString()
        {
            return $"{Broj}";
        }







    }
}
