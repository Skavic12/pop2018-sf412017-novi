﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;

namespace PrvaAplikacija.Models
{
    public class Korisnik : INotifyPropertyChanged ,ICloneable
    {

        public void Sacuvaj()
        {
            using(SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO KorisniciBaze(ime,prezime,Kor_ime,Loz,adresa,pol,tip_kor,status_kor,email)"+"VALUES(@Ime,@Prezime,@Korisnickoime,@Lozinka,@Adresa,@Pol,@Tipkorisnika,@Active,@Email)";

                command.Parameters.Add(new SqlParameter(@"Ime", this.Ime));
                command.Parameters.Add(new SqlParameter(@"Prezime",this.Prezime));
                command.Parameters.Add(new SqlParameter(@"Korisnickoime",this.Korisnickoime));
                command.Parameters.Add(new SqlParameter(@"Lozinka",this.Lozinka));
                command.Parameters.Add(new SqlParameter(@"Adresa",this.Adresa));
                command.Parameters.Add(new SqlParameter(@"Pol",this.Pol));
                command.Parameters.Add(new SqlParameter(@"Tipkorisnika",this.Tipkorisnika));
                command.Parameters.Add(new SqlParameter(@"Active",true));
                command.Parameters.Add(new SqlParameter(@"Email", this.Email));

                command.ExecuteNonQuery();


            }
            Database.Data.Instance.UcitaniKorisnici();

        }


        public void Izmena()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "UPDATE KorisniciBaze SET ime=@Ime , prezime=@Prezime, Kor_ime=@Korisnickoime , Loz=@Lozinka ,adresa=@Adresa,pol=@Pol,tip_kor=@Tipkorisnika,status_kor=@Active,email=@Email where id=@Id ";

                comm.Parameters.Add(new SqlParameter(@"Id", this.Id));
                comm.Parameters.Add(new SqlParameter(@"Ime", this.Ime));
                comm.Parameters.Add(new SqlParameter(@"Prezime", this.Prezime));
                comm.Parameters.Add(new SqlParameter(@"Korisnickoime", this.Korisnickoime));
                comm.Parameters.Add(new SqlParameter(@"Lozinka", this.Lozinka));
                comm.Parameters.Add(new SqlParameter(@"Adresa", this.Adresa));
                comm.Parameters.Add(new SqlParameter(@"Pol", this.Pol));
                comm.Parameters.Add(new SqlParameter(@"Tipkorisnika", this.Tipkorisnika));
                comm.Parameters.Add(new SqlParameter(@"Active", this.Active));
                comm.Parameters.Add(new SqlParameter(@"Email", this.Email));

                comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitaniKorisnici();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public Korisnik()
        {
            active = true;
        }

        private String ime;
        public String Ime
        {
            get { return ime; }
            set { ime = value; OnProperyChanged("Ime"); }
        }

        private String prezime;
        public String Prezime
        {
            get { return prezime; }
            set { prezime = value; OnProperyChanged("Prezime"); }
        }

        private String lozinka;
        public String Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; OnProperyChanged("Lozinka"); }
        }

        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("Id"); }
        }


        private String korisnickoime;
        public String Korisnickoime
        {
            get { return korisnickoime; }
            set { korisnickoime = value; OnProperyChanged("Korisnickoime"); }

        }

        private String pol;
        public String Pol
        {
            get { return pol; }
            set { pol = value; OnProperyChanged("Pol"); }
        }

        private String email;
        public String Email
        {
            get { return email; }
            set { email = value;OnProperyChanged("Email"); }
        }

        private String adresa;
        public String Adresa
        {
            get { return adresa; }
            set { adresa = value; OnProperyChanged("Adresa"); }
        }

        private String tipkorisnika;
        public String Tipkorisnika
        {
            get { return tipkorisnika; }
            set { tipkorisnika = value;OnProperyChanged("Tipkorisnika"); }
        }
        private bool active;
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

       public object Clone()
        {
            return new Korisnik
            {
                Id = this.Id,
                Ime = this.Ime,
                Prezime = this.Prezime,
                Korisnickoime = this.Korisnickoime,
                Lozinka = this.Lozinka,
                Pol = this.Pol,
                Adresa = this.Adresa,
                Tipkorisnika = this.Tipkorisnika,
                Active = this.Active,
                Email = this.Email
                

            };
            
        }

        public override string ToString()
        {
            return $"Ime {Ime},Prezime {Prezime},Korisnickoime{Korisnickoime},Lozink{Lozinka},Adresa{Adresa} ";
        }
    }
}
