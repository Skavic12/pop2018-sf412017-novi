﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data.SqlClient;

namespace PrvaAplikacija.Models
{
    public class Karte : INotifyPropertyChanged,ICloneable
    {

        public void Sacuvati()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO KarteBaza(sifraLeta,Kor_ime,ZauzetoSediste,Cena,tipKarte,S_komp,status_karte)"+
                                        "VALUES(@SifraLeta,@NazivPutnika,@ZauzetoSediste,@UkupnaCena,@TipKarte,@Kompanija,@Active)";


                
                command.Parameters.Add(new SqlParameter("@SifraLeta",this.SifraLeta));
                command.Parameters.Add(new SqlParameter("@NazivPutnika",this.NazivPutnika));
                command.Parameters.Add(new SqlParameter("@ZauzetoSediste",this.ZauzetoSediste));
                command.Parameters.Add(new SqlParameter("@UkupnaCena",this.UkupnaCena));
                command.Parameters.Add(new SqlParameter("@TipKarte",this.TipKarte));
                command.Parameters.Add(new SqlParameter("@Kompanija",this.Kompanija));
                command.Parameters.Add(new SqlParameter("@Active",true));

                command.ExecuteNonQuery();


            }

            Database.Data.Instance.UcitajKarte();
        }




        public event PropertyChangedEventHandler PropertyChanged;

        public void OnProperyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }

        }

        private int id;
        public int Id
        {
            get { return id; }
            set { id = value;OnProperyChanged("id"); }
        }

        
        private string sifraleta;
        public String SifraLeta
        {
            get { return sifraleta; }
            set { sifraleta = value;OnProperyChanged("SifraLeta"); }

        }

        private string nazivputnika;
        public String NazivPutnika
        {
            get { return nazivputnika; }
            set { nazivputnika = value; OnProperyChanged("NazivPutnika"); }
        }

        private string zauzetoSediste;
        public String ZauzetoSediste
        {
            get { return zauzetoSediste; }
            set { zauzetoSediste = value;OnProperyChanged("ZauzetoSediste"); }
        }

        private decimal ukupnaCena;
        public Decimal UkupnaCena
        {
            get { return ukupnaCena; }
            set { ukupnaCena = value; OnProperyChanged("UkupnaCena"); }

        }

        private string tipKarte;
        public String TipKarte
        {
            get { return tipKarte; }
            set { tipKarte = value; OnProperyChanged("TipKarte"); }
        }

        private bool active;

        public bool Active
        {
            get { return active; }
            set { active = value; }

        }

        private string kompanija;
        public String Kompanija
        {
            get { return kompanija; }
            set { kompanija = value; OnProperyChanged("Kompanija"); }
        }



        public Karte()
        {
            Active = true;
        }

        public object Clone()
        {
            return new Karte
            {
                Id = this.Id,
                SifraLeta = this.SifraLeta,
                NazivPutnika = this.NazivPutnika,
                ZauzetoSediste = this.ZauzetoSediste,
                TipKarte = this.TipKarte,
                UkupnaCena = this.UkupnaCena,
                Kompanija = this.Kompanija,
                Active = this.Active
            };
        }

    }



}

