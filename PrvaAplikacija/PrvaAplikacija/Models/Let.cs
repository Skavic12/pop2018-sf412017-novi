﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace PrvaAplikacija.Models
{
    public class Let : INotifyPropertyChanged,ICloneable
    {
        public void Sacuvati()
        {
            using(SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO LetoviB(s_let,odrediste,destinacija,Vreme_polaska,Vreme_dolaska,s_kom,cena,status_leta)"+"VALUES(@Sifra,@Odrediste,@Destinacija,@VremePolaska,@VremeDolaska,@AvioKOmpanija,@Cena,@Active)";

                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Odrediste", this.Odrediste));
                command.Parameters.Add(new SqlParameter("@Destinacija", this.Destinacija));
                command.Parameters.Add(new SqlParameter("@VremePolaska", this.VremePolaska));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", this.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@AvioKOmpanija", this.AvioKOmpanija));
                command.Parameters.Add(new SqlParameter("@Cena", this.Cena));
                command.Parameters.Add(new SqlParameter("@Active", true));

                command.ExecuteNonQuery();



            }
            Database.Data.Instance.UcitajSveLetove();
        }


        public void Izmena()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();
                SqlCommand comm = conn.CreateCommand();

                comm.CommandText = "update LetoviB set s_let=@Sifra , odrediste=@Odrediste,destinacija=@Destinacija,Vreme_polaska=@VremePolaska,Vreme_dolaska=@VremeDolaska,s_kom=@AvioKOmpanija,cena=@Cena ,status_leta=@Active where id=@id";

                comm.Parameters.Add(new SqlParameter(@"id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Odrediste", this.Odrediste));
                comm.Parameters.Add(new SqlParameter("@Destinacija", this.Destinacija));
                comm.Parameters.Add(new SqlParameter("@VremePolaska", this.VremePolaska));
                comm.Parameters.Add(new SqlParameter("@VremeDolaska", this.VremeDolaska));
                comm.Parameters.Add(new SqlParameter("@AvioKOmpanija", this.AvioKOmpanija));
                comm.Parameters.Add(new SqlParameter("@Cena", this.Cena));
                comm.Parameters.Add(new SqlParameter("@Active", this.Active));

                comm.ExecuteNonQuery();

            }
            catch(Exception ex)
            {
                Console.WriteLine($"Exception {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveLetove();

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnProperyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }

        }

        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("id"); }
        }


        private String sifra;

        public String Sifra
        {
            get { return sifra; }
            set { sifra = value;OnProperyChanged("Sifra"); }
        }

        private String avioKompanija;

        public String AvioKOmpanija
        {
            get { return avioKompanija; }
            set { avioKompanija = value; OnProperyChanged("AvioKOmpanija"); }
        }

        private String odrediste;
        public String Odrediste
        {
            get { return odrediste; }
            set { odrediste = value; OnProperyChanged("Odrediste"); }
        }


        private String destinacija;
        public String Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; OnProperyChanged("Destinacija"); }
        }

        private DateTime vremeDolaska;
        public DateTime VremeDolaska
        {
            get { return vremeDolaska; }
            set { vremeDolaska = value; OnProperyChanged("VremeDolaska"); }
        }

        private DateTime vremePolaska;

        public DateTime VremePolaska
        {
            get { return vremePolaska; }
            set { vremePolaska = value; OnProperyChanged("VremePolaska"); }
        }


        private decimal cena;



        public decimal Cena
        {
            get { return cena; }
            set { cena = value; OnProperyChanged("Cena"); }
        }

        

        private bool active;

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public object Clone()
        {
            return new Let
            {
                Sifra = this.Sifra,
                Destinacija = this.Destinacija,
                Odrediste = this.Odrediste,
                VremeDolaska = this.VremeDolaska,
                VremePolaska = this.VremePolaska,
                Cena = this.Cena,
                AvioKOmpanija = this.AvioKOmpanija,
                Active = this.Active
            };
        }





        public override string ToString()
        {
            return $"{Sifra} - {Destinacija} - {Odrediste}";
        }

        


    }
}
