﻿using PrvaAplikacija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Data;



namespace PrvaAplikacija.Database
{
    class Data
    {
        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<AvioKompanija> AvioKompanija { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Avion> Avions{get;set;}
        public ObservableCollection<Karte> Kartes { get; set; }
        public ObservableCollection<Sedista> Sedistas { get; set; }
        public String UlogovanKorisnik { get; set; }


        public ObservableCollection<Aerodrom> gradAerodrom { get; set; }

        public string ConnectionString
        {
            get
            {
                return CONNECTION_STRING;
            }
        }

        public const String CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=PodaciPrveAP;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private Data()
        {
            gradAerodrom = new ObservableCollection<Aerodrom>();
            AvioKompanija = new ObservableCollection<AvioKompanija>();
            Korisnici = new ObservableCollection<Korisnik>();
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Avions = new ObservableCollection<Avion>();
            Kartes = new ObservableCollection<Karte>();
            Sedistas = new ObservableCollection<Sedista>();
            UcitatiSveAerodrome();
            UcitajSveLetove();
            UcitaniKorisnici();
            UcitajSveAviokompanije();
            UcitajSveAvione();
            UcitajKarte();
            UcitajSedista();


        }

        private void UcitajSedista()
        {
            Sedistas.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT id, Red +''+ Broj AS Sediste  FROM IzgledSedista";

                SqlDataAdapter DA = new SqlDataAdapter();
                DA.SelectCommand = command;

                DataSet DS = new DataSet();
                DA.Fill(DS, "IzgledSedista");

                foreach (DataRow row in DS.Tables["IzgledSedista"].Rows)
                {
                    Sedista sedista = new Sedista();

                    sedista.Id = (int)row["id"];
                    sedista.Broj = (string)row["Sediste"];

                    Sedistas.Add(sedista);
                }
            }



        }

        public void UcitajKarte()
        {
            Kartes.Clear();
            using(SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT KarteBaza.id,sifraLeta,Kor_ime,ZauzetoSediste,Cena,tipKarte,a.naz_kom,status_karte FROM KarteBaza , AvioKompanijeBaze a
                                        WHERE a.S_kom = KarteBaza.S_komp";
                SqlDataAdapter DA = new SqlDataAdapter();
                DA.SelectCommand = command;

                DataSet DS = new DataSet();
                DA.Fill(DS, "KarteBaza");

                foreach(DataRow row in DS.Tables["KarteBaza"].Rows)
                {
                    Karte karte = new Karte();

                    karte.Id = (int)row["id"];
                    karte.SifraLeta = (string)row["sifraLeta"];
                    karte.NazivPutnika = (string)row["Kor_ime"];
                    karte.ZauzetoSediste = (string)row["ZauzetoSediste"];
                    karte.UkupnaCena = (Decimal)row["Cena"];
                    karte.TipKarte = (string)row["tipKarte"];
                    karte.Kompanija = (string)row["naz_kom"];
                    karte.Active = (bool)row["status_karte"];

                    Kartes.Add(karte);
                
                        
                 }

            }

        }

        public void UcitajSveAvione()
        {
            Avions.Clear();
            using(SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT a.id,pilot,a.s_leta,Sed_B,Sed_E,k.naz_kom,a.status FROM dbo.AvionBaza a,AvioKompanijeBaze k WHERE a.s_kom = k.S_kom";

                SqlDataAdapter DA = new SqlDataAdapter();
                DA.SelectCommand = command;

                DataSet DS = new DataSet();
                DA.Fill(DS, "AvionBaza");

                foreach(DataRow row in DS.Tables["AvionBaza"].Rows)
                {
                    Avion avion = new Avion();

                    avion.Id = (int)row["id"];
                    avion.Pilot = (string)row["pilot"];
                    avion.BrojLeta = (string)row["s_leta"];
                    avion.SedistaBiznisKlase = (string)row["Sed_B"];
                    avion.SedistaEkonomskeKlase = (string)row["Sed_E"];
                    avion.NazivAvioK = (string)row["naz_kom"];
                    avion.Aktivan = (bool)row["status"];

                    Avions.Add(avion);
                }
            }
        }

        public void UcitaniKorisnici()
        {
            Korisnici.Clear();
            using(SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM KorisniciBaze";
                SqlDataAdapter DAkorisnici = new SqlDataAdapter();
                DAkorisnici.SelectCommand = command;

                DataSet datasetkorisnici = new DataSet();
                DAkorisnici.Fill(datasetkorisnici, "KorisniciBaze");
                
                foreach(DataRow row in datasetkorisnici.Tables["KorisniciBaze"].Rows)
                {
                    Korisnik korisnik = new Korisnik();

                    korisnik.Id = (int)row["id"];
                    korisnik.Ime = (string)row["ime"];
                    korisnik.Prezime = (string)row["prezime"];
                    korisnik.Korisnickoime = (string)row["Kor_ime"];
                    korisnik.Lozinka = (string)row["Loz"];
                    korisnik.Adresa = (string)row["adresa"];
                    korisnik.Pol = (string)row["pol"];
                    korisnik.Tipkorisnika = (string)row["tip_kor"];
                    korisnik.Active = (bool)row["status_kor"];
                    korisnik.Email = (string)row["email"];

                    Korisnici.Add(korisnik);

                }
            }
        }

    
        public void UcitajSveLetove()
        {
            Letovi.Clear();
            using(SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT  LetoviB.id ,s_let , odrediste,destinacija,Vreme_polaska,Vreme_dolaska,cena , naz_kom , LetoviB.status_leta
                                        FROM LetoviB , AvioKompanijeBaze
                                        WHERE LetoviB.s_kom = AvioKompanijeBaze.s_kom ";

                SqlDataAdapter daaletovi = new SqlDataAdapter();
                daaletovi.SelectCommand = command;
                DataSet DSletovi = new DataSet();
                daaletovi.Fill(DSletovi, "LetoviB");
                foreach(DataRow row in DSletovi.Tables["LetoviB"].Rows)
                {
                    Let let = new Let();

                    let.Id = (int)row["id"];
                    let.Sifra = (string)row["s_let"];
                    let.Odrediste = (string)row["odrediste"];
                    let.Destinacija = (string)row["destinacija"];
                    let.VremePolaska = (DateTime)row["Vreme_polaska"];
                    let.VremeDolaska = (DateTime)row["Vreme_dolaska"];
                    let.AvioKOmpanija = (string)row["naz_kom"];
                    let.Cena = (Decimal)row["cena"];
                    let.Active = (bool)row["status_leta"];

                    Letovi.Add(let);



                }


            }
        }

        public void UcitajSveAviokompanije()
        {
            AvioKompanija.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM AvioKompanijeBaze";
                SqlDataAdapter DAAviokomp = new SqlDataAdapter();
                DAAviokomp.SelectCommand = command;

                DataSet DSavioKompanija = new DataSet();
                DAAviokomp.Fill(DSavioKompanija, "AvioKompanijaBaze");

                foreach (DataRow row in DSavioKompanija.Tables["AvioKompanijaBaze"].Rows)
                {
                    AvioKompanija avioKompanijas = new AvioKompanija();

                    avioKompanijas.Id = (int)row["id"];
                    avioKompanijas.KompaniSifra = (string)row["S_kom"];
                    avioKompanijas.NazivKompanije = (string)row["naz_kom"];
                    avioKompanijas.Active = (bool)row["status"];

                    AvioKompanija.Add(avioKompanijas);


                }
            }

        }

        private static Data _instance = null;
        public static Data Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Data();
                return _instance;
            }
        }
        public void UcitatiSveAerodrome()
        {

            Aerodromi.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM AerodromiBaze";

                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                daAerodromi.SelectCommand = command;

                System.Data.DataSet dsAerodromi = new System.Data.DataSet();
                daAerodromi.Fill(dsAerodromi, "AerodromiBaze");

                foreach (DataRow row in dsAerodromi.Tables["AerodromiBaze"].Rows)
                {
                    Aerodrom aerodrom = new Aerodrom();

                    aerodrom.Id = (int)row["id"];
                    aerodrom.Sifra = (string)row["S_aeo"];
                    aerodrom.Naziv = (string)row["Naziv"];
                    aerodrom.Grad = (string)row["Grad"];
                    aerodrom.Active = (bool)row["status"];
                    

                    Aerodromi.Add(aerodrom);



                }
            }
            
            
            
            
            /*
            XmlReader reader = XmlReader.Create("..//..//Data//Aerodromi.xml");

            while (reader.Read())
            {
                if (reader.NodeType.Equals(XmlNodeType.Element) && reader.Name.Equals("aerodrom"))
                {


                    string sifra = reader.GetAttribute("sifra");
                    string naziv = reader.GetAttribute("naziv");
                    string grad = reader.GetAttribute("grad");
                    string active = reader.GetAttribute("active");
                    bool boolactive = Convert.ToBoolean(active); 
                    
                    Aerodromi.Add(new Aerodrom { Sifra =sifra, Naziv=naziv,Grad=grad, Active = boolactive
                    
                    });
                    gradAerodrom.Add(new Aerodrom { Grad = grad });
                }
            }
            reader.Close();
            */
        }
        public void SacuvajAerodrome()
        {
            XmlWriter writer = XmlWriter.Create("..//..//Data//Aerodromi.xml");
            writer.WriteStartElement("aerodromi");
            foreach (var aerodrom in Aerodromi)
            {
                writer.WriteStartElement("AERODROM");
                writer.WriteAttributeString("sifra", aerodrom.Sifra);
                writer.WriteAttributeString("naziv", aerodrom.Naziv);
                writer.WriteAttributeString("grad", aerodrom.Grad);
                writer.WriteEndElement();
            }
            writer.WriteEndDocument();
            writer.Close();
        }
  /*      
        public void SacuvajKorisnike()
        {
            XmlWriter writer = XmlWriter.Create("..//..//Data//Korisnici.xml");
            writer.WriteStartElement("korisnic");
            foreach (var korisnik in Korisnici)
            {
                writer.WriteStartElement("korisnik");
                writer.WriteAttributeString("ime", korisnik.Ime);
                writer.WriteAttributeString("prezime", korisnik.Prezime);
                writer.WriteAttributeString("lozinka", korisnik.Lozinka);
                writer.WriteAttributeString("Korisnickoime", korisnik.Korisnickoime);
                writer.WriteAttributeString("pol", korisnik.Pol.ToString());
                writer.WriteAttributeString("adresa", korisnik.Adresa);
                writer.WriteAttributeString("tipKorisnika", korisnik.Tipkorisnika.ToString());
                writer.WriteAttributeString("obrisano", korisnik.Active.ToString());
                writer.WriteEndElement();


            }
            writer.WriteEndDocument();
            writer.Close();
        }
        public void UcitajSveKorisnike()
        {
            XmlReader reader = XmlReader.Create("..//..//Data//Korisnici.xml");

            while (reader.Read())
            {
                if (reader.NodeType.Equals(XmlNodeType.Element) && reader.Name.Equals("korisnici"))
                {
                    var korisnik = new Korisnik
                    {
                        Ime = reader.GetAttribute("ime"),
                        Prezime = reader.GetAttribute("prezime"),
                        Lozinka = reader.GetAttribute("lozinka"),
                        Korisnickoime = reader.GetAttribute("korisnickoime"),
                        Pol = (reader.GetAttribute("pol")),
                        Adresa = reader.GetAttribute("adresa"),
                        Tipkorisnika = (reader.GetAttribute("tipkorisnika")),
                        Active = Convert.ToBoolean(reader.GetAttribute("active"))
                    };
                    //Console.WriteLine(korisnik);
                    Korisnici.Add(korisnik);
                }
            }
            reader.Close();

        }
        
        public void UcitajSveLetove()
        {
        Letovi.Add(new Let
            {
                Sifra = "123",
                Destinacija = "BEG",
                Odrediste = "LON",
                VremeDolaska = new DateTime(2018, 11, 1, 1, 00, 00),
                VremePolaska = new DateTime(2018, 10, 1, 22, 00, 00),
                Cena = 15000,
                AvioKOmpanija = 1,
                Active = true
            });
            Letovi.Add(new Let
            {
                Sifra = "1234",
                Destinacija = "BEG",
                Odrediste = "MIL",
                VremeDolaska = new DateTime(2018, 12, 1, 11, 00, 00),
                VremePolaska = new DateTime(2018, 12, 1, 9, 00, 00),
                Cena = 13000,
                AvioKOmpanija = 1,
                Active = true
            });
            Letovi.Add(new Let
            {
                Sifra = "12345",
                Destinacija = "BEG",
                Odrediste = "AMS",
                VremeDolaska = new DateTime(2018, 4, 2, 17, 00, 00),
                VremePolaska = new DateTime(2018, 4, 2, 15, 00, 00),
                Cena = 12000,
                AvioKOmpanija = 1,
                Active = true
            });

        }
        */

        public void SacuvajSveAerodrome()
        {
            XmlWriter writer = XmlWriter.Create("..//..//Data//Aerodromi.xml");

            writer.WriteStartElement("aerodromi");
            foreach (var aerodrom in Aerodromi)
            {
                writer.WriteStartElement("aerodrom");
                writer.WriteAttributeString("sifra", aerodrom.Sifra);
                writer.WriteAttributeString("naziv", aerodrom.Naziv);
                writer.WriteAttributeString("grad", aerodrom.Grad);
                writer.WriteEndElement();
            }

            writer.WriteEndDocument();
            writer.Close();
        }
    }
}
